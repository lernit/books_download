import urllib.request

import requests, re, os
from urllib.parse import urlparse, urljoin
from bs4 import BeautifulSoup, SoupStrainer
import colorama
import httplib2

class Downloader():

    count = 0

    def get_links(cls, url, lstart):
        http = httplib2.Http()
        status, response = http.request(url)
        links = []
        for a in BeautifulSoup(response, parse_only=SoupStrainer('a')):
            link = re.findall(r'"([^"]*)"', str(a))
            for l in link:
                if l.startswith(lstart):
                    links.append(l)

        return links

    def get_dlinks(cls, url, lstart1, lstart2):
        dlinks = []
        for link in cls.get_links(url, lstart1):
            links = cls.get_links(link, lstart2)
            dlinks.append(links[0]) if len(links) > 0 else []
        return dlinks

    def get_paginated_links(cls, req):
        urls = []
        for page in range(10):
            url = f'http://libgen.lc/search.php?&res=100&req={req}&phrase=1&view=simple&column=def&sort=def&sortmode=ASC&page={page+1}'
            urls.append(url)
        return urls

    def get_all_links(cls, req, lstart1, lstart2):
        links = []
        plinks = cls.get_paginated_links(req)
        for plink in plinks:
            dlinks = cls.get_dlinks(plink, lstart1, lstart2)
            for link in dlinks:
                links.append(link)
        return links

    def get_links_from_reqlist(cls, reqlist, lstart1, lstart2):
        links = []
        for req in reqlist:
            req_links = cls.get_all_links(req, lstart1, lstart2)
            for link in req_links:
                links.append(link)
        return list(set(links))

    #all_links = get_links_from_reqlist(reqs1, mylstart1, mylstart2)

    def links_to_file(cls, links, file):
        with open(file, "a") as link:
            for url in links:
                link.write("%s\n" % url)


    def download_file(cls, link, dirpath):
        cls.count += 1
        filename = urlparse(link).path
        filepath = dirpath + '/' + urllib.parse.unquote(os.path.basename(filename))
        print(cls.count, filepath)
        if not os.path.isfile(filepath):
            try:
                urllib.request.urlretrieve(link, filepath)
            except:
                print('File could not be downloaded to:', filepath)

    def download_files(cls, reqlist, lstart1, lstart2, down_dir):
        flinks = cls.get_links_from_reqlist(reqlist, lstart1, lstart2)
        cls.links_to_file(flinks, down_dir + '/FILES_TO_DOWNLOAD.txt')
        for link in flinks:
            cls.download_file(link, down_dir)

    def download_files_from_file(cls, filename):
        with open(filename) as f:
            content = f.readlines()
        for link in [x.strip() for x in content]:
            cls.download_file(link, down_dir)

flink = 'http://31.42.184.140/main/2000/d353de1992a811d58a30d385b9196b7b/%28SuDoc%20NAS%201.21_7084%29%20Mary%20K.%20McCaskill%20-%20Grammar%2C%20punctuation%2C%20and%20capitalization_%20a%20handbook%20for%20technical%20writers%20and%20editors-National%20Aeronautics%20and%20Space%20Administration%2C%20Office%20of%20Management%2C%20Sc.pdf'

reqs1 = ['mole', 'test', 'theory', 'electric', 'atom']
reqs2 = ['animals', 'biology', 'chemistry', 'laws', 'rules']
reqs3 = ['police', 'science', 'wood', 'equipment', 'weapon']
reqs4 = ['holiday', 'transport', 'medicine', 'energy', 'politics']
reqs5 = ['history', 'building', 'development', 'administration', 'earth']
reqs6 = ['nature', 'sport', 'state', 'insect', 'product']

lstart1 = 'http://93'
lstart2 = 'http://31'
down_dir = os.path.abspath("/home/spacer/libfiles")
file_path = os.path.abspath("/home/spacer/libfiles/FILES_TO_DOWNLOAD.txt")
dfiles = Downloader()

dfiles.download_files_from_file(file_path)
# dfiles.download_files(reqs1, lstart1, lstart2, down_dir)
# dfiles.download_files(reqs2, lstart1, lstart2, down_dir)
# dfiles.download_files(reqs3, lstart1, lstart2, down_dir)
# dfiles.download_files(reqs4, lstart1, lstart2, down_dir)
# dfiles.download_files(reqs5, lstart1, lstart2, down_dir)
# dfiles.download_files(reqs6, lstart1, lstart2, down_dir)